# python-boilerplate

A place to keep python code I have written for some python projects.

```
Using Python 3.11.2
```

# Instructions to run code can be found in the README files in directories.

```
parse_normalize_json/README.md
```
