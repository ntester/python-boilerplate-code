import logging
import json
import uuid
import arrow
import pytz
import re
import yaml

import os

from datetime import datetime, timedelta
from pyisemail import is_email

logger = logging.getLogger(__name__)


class FlmNormalize:

    def __init__(self, check=None):
        
        self.check = check
        self.audio_non_security_devices = []
        logger.info('We have our class')
        config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),'config')

        rules_file_path =  os.path.join(config_path, 'normalize_rules.yml')
        aaa_file_path = os.path.join(config_path, 'aam_rules.yml')
        dcip_file_path = os.path.join(config_path, 'dcip_rules.yml')
        kinepolis_file_path = os.path.join(config_path, 'kinepolis_rules.yml')
        cert_db_file_path = os.path.join(config_path, 'cert-db_rules.yml')

        with open(rules_file_path, 'r') as my_normalize_rules:
            self.normalize_settings = yaml.load(my_normalize_rules,Loader=yaml.FullLoader)

        with open(aaa_file_path, 'r') as my_aam_rules:
            self.aam_rules = yaml.load(my_aam_rules, Loader=yaml.FullLoader)

        with open(dcip_file_path, 'r') as my_dcip_rules:
            self.dcip_rules = yaml.load(my_dcip_rules, Loader=yaml.FullLoader)

        with open(kinepolis_file_path, 'r') as my_kinepolis_rules:
            self.kinepolis_rules = yaml.load(my_kinepolis_rules, Loader=yaml.FullLoader)

        with open(cert_db_file_path, 'r') as my_cert_db_rules:
            self.cert_db_rules = yaml.load(my_cert_db_rules, Loader=yaml.FullLoader)

        self.integrator_rules = self.cert_db_rules.copy()

    def get_circuit(self, circuit_dict):
        circuit = self.get_user_text_variable(circuit_dict.get('Circuit', None))
        if not circuit:
            return 'None'
        if 'NONE' in circuit.upper():
            return 'None'
        else:
            return circuit

    def get_facility_info(self, facility_info):
        complete_facility_info = dict()

        complete_facility_info['FacilityName'] = self.get_user_text_variable(facility_info.get('FacilityName', None))
        complete_facility_info['Circuit'] = self.get_circuit(facility_info)
        complete_facility_info['Addresses'] = self.get_address_list(facility_info.get('AddressList', None))
        complete_facility_info['AlternateFacilityIDList'] = self.get_alternate_facility_id_list(
            facility_info.get('AlternateFacilityIDList', None))
        complete_facility_info['FacilityCapabilities'] = self.get_facility_capabilities(
            facility_info.get('Capabilities', None))
        complete_facility_info['ContactList'] = self.get_contact_list(facility_info.get('ContactList', None))
        complete_facility_info['FacilityDeviceList'] = self.get_facility_device_list(
            facility_info.get('DeviceList', None))
        complete_facility_info['FacilityTimeZone'] = self.get_facility_time_zone(
            facility_info.get('FacilityTimeZone', None))

        return complete_facility_info

    def get_contact_list(self, facility_contact_list):
        facility_contact = []
        # logger.info ('in the class get_contact_list')
        if not facility_contact_list:
            return facility_contact
        for contact in facility_contact_list['Contact']:
            contact_dict = dict()
            contact_dict['Name'] = self.get_user_text_variable(contact.get('Name', None))
            contact_dict['CountryCode'] = contact.get('CountryCode', None)
            contact_dict['Phone1'] = contact.get('Phone1', None)
            contact_dict['Phone2'] = contact.get('Phone2', None)
            email_address = contact.get('Email', None)
            # Validate email before we accept
            if email_address and is_email(email_address):
                contact_dict['Email'] = email_address
            else:
                logger.info('EMAIL address [{0}] is NOT VALID'.format(email_address))

            contact_dict['Type'] = contact.get('Type', None)
            facility_contact.append(contact_dict.copy())
        return facility_contact

    def get_facility_device_list(self, facility_device_list):
        facility_device = []
        if not facility_device_list:
            return facility_device

        facility_device = self.get_devices(facility_device_list.get('Device', None))
        return facility_device

    def get_facility_capabilities(self, facility_capabilities):
        facility_capabilities_dict = dict()
        facility_capabilities_dict['DCPDeliveryMethodList'] = {}
        facility_capabilities_dict['KDMDeliveryMethodList'] = {}
        if not facility_capabilities:
            return facility_capabilities_dict

        facility_capabilities_dict['KDMDeliveryMethodList'] = self.get_interop_delivery_methods(
            facility_capabilities.get('KDMDeliveryMethodList', None))
        facility_capabilities_dict['DCPDeliveryMethodList'] = self.get_interop_delivery_methods(
            facility_capabilities.get('DCPDeliveryMethodList', None))

        return facility_capabilities_dict

    def get_smpte_delivery_methods(self, delivery_method_list):
        logger.info('In the class get_delivery_methods')
        facility_delivery_methods = {}

        if not delivery_method_list:
            return facility_delivery_methods

        email_final_list = []
        modem_final_list = []
        network_final_list = []
        physical_final_list = []
        satellite_final_list = []

        # Email, Modem, Network, Physical , Satellite, TKR , ##other"
        if 'Email' in delivery_method_list['DeliveryMethod']:
            email_list = delivery_method_list['DeliveryMethod']['Email']
            for email in email_list:
                email_dict = dict()
                email_dict['EmailName'] = email.get('EmailName', None)
                email_address = email.get('EmailAddress', None)

                # Validate email before we accept
                if email_address:
                    if is_email(email_address):
                        email_dict['EmailAddress'] = email_address
                    else:
                        logger.info('EMAIL address [{0}] is NOT VALID'.format(email_address))
                        email_dict = dict()
                else:
                    logger.info('EMAIL address [{0}] is NOT VALID'.format(email_address))
                    email_dict = dict()
                if email_dict:
                    email_final_list.append(email_dict.copy())

        facility_delivery_methods['Email'] = email_final_list

        if 'Modem' in delivery_method_list['DeliveryMethod']:
            modem_list = delivery_method_list['DeliveryMethod']['Modem']
            for modem in modem_list:
                modem_dict = dict()
                modem_dict['PhoneNumber'] = modem.get('PhoneNumber', None)
                modem_final_list.append(modem_dict.copy())

        facility_delivery_methods['Modem'] = modem_final_list

        if 'Network' in delivery_method_list['DeliveryMethod']:
            network_list = delivery_method_list['DeliveryMethod']['Network']
            for network in network_list:
                network_dict = dict()
                network_dict['URL'] = network.get('URL', None)
                network_final_list.append(network_dict.copy())

        facility_delivery_methods['Network'] = network_final_list

        if 'Physical' in delivery_method_list['DeliveryMethod']:
            physical_list = delivery_method_list['DeliveryMethod']['Physical']
            for physical in physical_list:
                physical_dict = dict()
                physical_dict['MediaType'] = physical.get('MediaType', None)
                physical_dict['Detail'] = physical.get('Detail', None)
                physical_final_list.append(physical_dict.copy())

        facility_delivery_methods['Physical'] = physical_final_list

        if 'Satellite' in delivery_method_list['DeliveryMethod']:
            satellite_list = delivery_method_list['DeliveryMethod']['Satellite']
            for satellite in satellite_list:
                satellite_dict = dict()
                satellite_dict['Provider'] = satellite.get('Provider', None)
                satellite_final_list.append(satellite_dict.copy())

            facility_delivery_methods['Satellite'] = satellite_final_list

        if 'TKR' in delivery_method_list['DeliveryMethod']:
            facility_delivery_methods['TKR'] = self.get_boolean_value(
                delivery_method_list['DeliveryMethod'].get('TKR', None))
        else:
            facility_delivery_methods['TKR'] = None

        return facility_delivery_methods

    def get_interop_delivery_methods(self, delivery_method_list):
        logger.info('In the class get_list_delivery_methods')
        facility_delivery_methods = {}

        if not delivery_method_list:
            return facility_delivery_methods

        # Check if we are using SMPTE standards
        # we have to check if the delievery method is sent interop or smpte standard
        if isinstance(delivery_method_list['DeliveryMethod'], dict):
            return self.get_smpte_delivery_methods(delivery_method_list)

        email_final_list = []
        modem_final_list = []
        network_final_list = []
        physical_final_list = []
        satellite_final_list = []
        # loop through
        # Email, Modem, Network, Physical , Satellite, TKR , ##other"

        for delivery_method in delivery_method_list['DeliveryMethod']:
            if 'Email' in delivery_method:
                email_list = delivery_method['Email']
                email_dict = dict()
                email_dict['EmailName'] = email_list.get('EmailName', None)
                email_address = email_list.get('EmailAddress', None)

                # Validate email before we accept
                if email_address:
                    if is_email(email_address):
                        email_dict['EmailAddress'] = email_address
                    else:
                        logger.info('EMAIL address [{0}] is NOT VALID'.format(email_address))
                        email_dict = dict()
                else:
                    logger.info('EMAIL address [{0}] is NOT VALID'.format(email_address))
                    email_dict = dict()
                if email_dict:
                    email_final_list.append(email_dict.copy())

            facility_delivery_methods['Email'] = email_final_list

            if 'Modem' in delivery_method:
                modem_list = delivery_method['Modem']
                modem_dict = dict()
                modem_dict['PhoneNumber'] = modem_list.get('PhoneNumber', None)
                modem_final_list.append(modem_dict.copy())

            facility_delivery_methods['Modem'] = modem_final_list

            if 'Network' in delivery_method:
                network_list = delivery_method['Network']
                network_dict = dict()
                network_dict['URL'] = network_list.get('URL', None)
                network_final_list.append(network_dict.copy())

            facility_delivery_methods['Network'] = network_final_list

            if 'Physical' in delivery_method:
                physical_list = delivery_method['Physical']
                physical_dict = dict()
                physical_dict['MediaType'] = physical_list.get('MediaType', None)
                physical_dict['Detail'] = physical_list.get('Detail', None)
                physical_final_list.append(physical_dict.copy())

            facility_delivery_methods['Physical'] = physical_final_list

            if 'Satellite' in delivery_method:
                satellite_list = delivery_method['Satellite']
                satellite_dict = dict()
                satellite_dict['Provider'] = satellite_list.get('Provider', None)
                satellite_final_list.append(satellite_dict.copy())

            facility_delivery_methods['Satellite'] = satellite_final_list

            if 'TKR' in delivery_method:
                facility_delivery_methods['TKR'] = self.get_boolean_value(delivery_method.get('TKR', None))

        return facility_delivery_methods

    def get_address_list(self, address_dict):
        addresslist_dict = {}

        if not address_dict:
            return addresslist_dict

        addresslist_dict['Physical'] = self.get_address(address_dict.get('Physical', None))
        addresslist_dict['Shipping'] = self.get_address(address_dict.get('Shipping', None))
        addresslist_dict['Billing'] = self.get_address(address_dict.get('Billing', None))

        return addresslist_dict

    # generic way to get the address values for shipping, physical, and billing
    def get_address(self, address_dict):
        logger.info('We have our get_address in the class')
        facility_address = {}
        if address_dict:
            # TODO need to fix data
            # Addressee,StreetAddress,StreetAddress2,City,Province,PostalCode,Country (flm:ISO3166CountyCode)
            facility_address['Addressee'] = address_dict.get('Addressee', None)

            facility_address['City'] = address_dict.get('City', None)

            facility_address['Province'] = address_dict.get('Province', None)

            facility_address['PostalCode'] = address_dict.get('PostalCode', None)

            facility_address['Country'] = address_dict.get('Country', None)

            facility_address['StreetAddress'] = self.get_user_text_variable(address_dict.get('StreetAddress', None))
            facility_address['StreetAddress2'] = self.get_user_text_variable(address_dict.get('StreetAddress2', None))

        return facility_address

    def get_alternate_facility_id_list(self, alternate_id_list):
        alternate_list = []
        if not alternate_id_list:
            return alternate_list
        logger.info('We have our get_alternate_facility_id_list in the class')
        # TODO do we need to validate the data?
        if 'AlternateFacilityID' in alternate_id_list:
            # loop the list
            for alternate_id in alternate_id_list['AlternateFacilityID']:
                alternate_id_dict = dict()
                alternate_id_dict['AlternateFacilityID'] = alternate_id
                alternate_list.append(alternate_id_dict.copy())

        return alternate_list

    def get_facility_time_zone(self, facility_time_zone):
        logger.info('We have our get_facility_time_zone in the class')
        if facility_time_zone in pytz.all_timezones:
            return facility_time_zone
        else:
            return 'Etc/UTC'

    def format_date_string(self, data):
        epoc = '1970-01-01'
        delimiters = [".", "-"]
        if data:
            # check for - as the delimiter with 2 char year
            if data[-3] == "-" and data[2] == "-" and len(data[3:-3]) == 3:
                return arrow.get(data, 'DD-MMM-YY').floor('second').datetime.strftime("%Y-%m-%d")
            # check for - as the delimiter with full 4 char year
            elif data[-5] == "-" and data[2] == "-" and len(data[3:-5]) == 3:
                return arrow.get(data, 'DD-MMM-YYYY').floor('second').datetime.strftime("%Y-%m-%d")
            # check for . as the delimiter xx.xx.xxxx
            # check for - as the delimiter xx-xx-xxxx
            elif data[-5] in delimiters and data[2] in delimiters:
                split_data = [int(x) for x in re.split('[-.]', data)]
                if split_data[0] <= 12 and (split_data[1] <= 12 or split_data[1] > 12):
                    return arrow.get(data, 'MM.DD.YYYY').floor('second').datetime.strftime("%Y-%m-%d")
                elif split_data[0] > 12 and split_data[1] <= 12:
                    return arrow.get(data, 'DD.MM.YYYY').floor('second').datetime.strftime("%Y-%m-%d")
                else:
                    return epoc

            # check for . as the delimiter xxxx.xx.xx
            # check for - as the delimiter xxxx-xx-xx
            elif data[-3] in delimiters and data[4] in delimiters:
                split_data = [int(x) for x in re.split('[-.]', data)]
                if split_data[1] <= 12 and (split_data[2] <= 12 or split_data[2] > 12):
                    return arrow.get(data, 'YYYY.MM.DD').floor('second').datetime.strftime("%Y-%m-%d")
                elif split_data[1] > 12 and split_data[2] <= 12:
                    return arrow.get(data, 'YYYY.DD.MM').floor('second').datetime.strftime("%Y-%m-%d")
                else:
                    return epoc
            else:
                return epoc
        else:
            return data

    def get_issue_date(self, issuedate):

        if not issuedate:
            format_issue_date = arrow.utcnow()
        else:
            format_issue_date = arrow.get(issuedate).floor('second').to('UTC')
        return str(format_issue_date.strftime("%Y-%m-%dT%H:%M:%S"))

    def get_complete_venue(self, flm_facility_message):
        complete_venue_info = {}
        if not flm_facility_message:
            return complete_venue_info

        if 'FacilityInfo' in flm_facility_message:
            complete_venue_info['FacilityID'] = self.get_normalized_facility_id(
                flm_facility_message['FacilityInfo'].get('FacilityID', None))
            complete_venue_info['Source'] = self.set_integrator(complete_venue_info['FacilityID'])
        else:
            # TODO Need to log this
            complete_venue_info['FacilityID'] = 'Not Found'
            self.integrator_rules = self.cert_db_rules.copy()
            logger.info('FacilityID not found on this FLM [{0}]'.format(flm_facility_message))

        complete_venue_info['IssueDate'] = self.get_issue_date(flm_facility_message.get('IssueDate', None))
        complete_venue_info['MessageId'] = flm_facility_message.get('MessageId', None)
        complete_venue_info['AnnotationText'] = self.get_user_text_variable(
            flm_facility_message.get('AnnotationText', None))
        return complete_venue_info

    def get_normalized_facility_id(self, facility_id):
        facility_id_normalized = facility_id.replace('urn:x-facilityID:', '')
        return facility_id_normalized

    def set_integrator(self, facility_id):
        # Get the intergrator from the FacilityID.
        # ID contains the integrater.  Example format below
        # "FacilityID": "urn:x-facilityID:aam.com:ES-YLO-002001-01"

        integrator_rules = ''
        facility_id_integrator = facility_id.replace('urn:x-facilityID:', '').split(':')[0]

        # get integrator from normailze rules file
        integrator_values = self.normalize_settings.get('integrators')
        for integrator in integrator_values:
            if facility_id_integrator in integrator_values[integrator]:
                integrator_rules = integrator
                break

        # Map integrator to correct rules.
        if integrator_rules == 'kinepolis':
            self.integrator_rules = self.kinepolis_rules.copy()
        elif integrator_rules == 'aam':
            self.integrator_rules = self.aam_rules.copy()
        elif integrator_rules == 'dcip':
            self.integrator_rules = self.dcip_rules.copy()
        elif integrator_rules == 'eikon':
            self.integrator_rules = self.cert_db_rules.copy()
        else:
            self.integrator_rules = self.cert_db_rules.copy()
            integrator_rules = 'unhandled integrator'

        return integrator_rules

    def get_auditorium_info(self, auditorium_info):
        complete_auditorium_info = []
        complete_auditorium_info_dict = {}

        if not auditorium_info:
            complete_auditorium_info_dict['AuditoriumList'] = []
            return complete_auditorium_info_dict

        for auditorium in auditorium_info['Auditorium']:
            auditorium_detail = dict()

            auditorium_detail['AuditoriumNumberOrName'] = auditorium.get('AuditoriumNumberOrName', None)
            auditorium_detail['AuditoriumInstallDate'] = self.format_date_string(auditorium.get('AuditoriumInstallDate', None))
            auditorium_detail['SeatingCapacity'] = auditorium.get('SeatingCapacity', None)
            auditorium_detail['ScreenWidth'] = self.get_screenwidth(auditorium.get('ScreenWidth', None))
            auditorium_detail['Capabilities'] = self.get_auditorium_capabilities(auditorium.get('Capabilities', None))
            auditorium_detail['SuiteList'] = self.get_suite_list(auditorium.get('SuiteList', None))
            auditorium_detail['NonSecurityDeviceList'] = self.get_non_security_device_list(
                auditorium.get('NonSecurityDeviceList', None))
            complete_auditorium_info.append(auditorium_detail.copy())

        complete_auditorium_info_dict['AuditoriumList'] = complete_auditorium_info
        return complete_auditorium_info_dict

    # TODO needs rule file (COMPLETE)
    def get_screenwidth(self, screen_width):
        logger.info('We have our get_screenwidth in the class')
        length_unit_values = self.normalize_settings.get('length_unit_values')
        screenwidth_dict = {}
        screenwidth_dict['Units'] = None
        screenwidth_dict['Value'] = None

        if screen_width:
            if '@units' in screen_width:
                # TODO validate the units given to a outside file)
                if screen_width['@units'].lower() in length_unit_values:
                    screenwidth_dict['Units'] = screen_width['@units'].lower()
                else:
                    screenwidth_dict['Units'] = ''

            screenwidth_dict['Value'] = screen_width.get('#value', None)

        return screenwidth_dict

    # TODO needs rule file (COMPLETE)
    def get_screen_aspect_ratio(self, screen_aspect_ratio):
        logger.info('We have our get_screen_aspect_ratio in the class')

        screen_aspect_ratio_values = self.normalize_settings.get('screen_aspect_ratio_values')

        screen_aspect_ratio_dict = {}
        screen_aspect_ratio_dict['Value'] = None
        if screen_aspect_ratio:
            for ratio in screen_aspect_ratio_values:
                if screen_aspect_ratio in screen_aspect_ratio_values[ratio]:
                    screen_aspect_ratio_dict['Value'] = ratio
                    break
                else:
                    screen_aspect_ratio_dict['Value'] = 'Other'

        return screen_aspect_ratio_dict

    # TODO needs rule file (COMPLETE)
    def get_adjustable_screen_mask(self, ad_screen_mask):
        logger.info('We have our get_adjustable_screen_mask in the class')
        # Valid AdjustableScreenMaskTypes
        # value="Top"
        # value="Side"
        # value="Bottom"
        # value="FloatingScope"
        # value="FloatingFlat"
        # value="SideBottom"
        # value="SideTop"
        # value="TopBottom"
        # value="All"
        # value="Fixed"
        screen_mask_value = ''

        adjustable_screen_mask_values = self.normalize_settings.get('adjustable_screen_mask_values')

        # Need to make this a scoped variable check.
        adjustable_screen_mask = self.get_scoped_variable(ad_screen_mask)

        if not adjustable_screen_mask:
            return None

        if adjustable_screen_mask:
            for screen_mask in adjustable_screen_mask_values:

                if adjustable_screen_mask.upper() in adjustable_screen_mask_values[screen_mask]:
                    return screen_mask

                else:
                    screen_mask_value = None
        return screen_mask_value

    # TODO: clean this function up (nicole)
    # TODO needs rule file
    def get_audioformatlist(self, audioformat_list):
        logger.info('We have our get_audioformatlist in the class')
        auditorium_audioformatlist = []
        audioformat_set = set()
        if not audioformat_list:
            return auditorium_audioformatlist

        # These values should not be in the FLM as AudioFormat, if we see them,
        # we create these values for AudioFormat and make a NonSecurityDeviceList entry
        # if atmos -> 51, M
        # if auro -> 51, M
        # if dts:x -> 51, M

        # if 71 -> 51, M
        # if 61 -> 51, M
        # if SDS -> 51, M
        # if 51 -> M
        # if M -> M

        # to make a list unique
        # my_list = list(set(my_list))

        audio_format_values = self.normalize_settings.get('audio_format_values')
        complete_audio_format = self.normalize_settings.get('associated_audio_format')
        audio_non_security_device = self.normalize_settings.get('audio_non_security_device')

        for audioformat_value in audioformat_list['AudioFormat']:
            audioformat_dict = {}

            # first normalize the data
            value = self.get_scoped_variable(audioformat_value)

            # check to see if we need to create a NonSecDevice
            if value.upper() in audio_non_security_device:
                # create non-security-device
                self.add_audio_non_security_device(value.upper())

            # check if our value in a valid audio_format
            for audio_format in audio_format_values:
                if value.strip().upper() in audio_format_values[audio_format]:
                    audioformat_dict['Value'] = audio_format
                    audioformat_set.add(audio_format)
                    break
                else:
                    audioformat_dict['Value'] = 'other'

        tmp_list = [a for a in sorted(audioformat_set)]
        # loop to get all values associated with sound
        for audio in tmp_list:
            auditorium_audioformatlist.extend(complete_audio_format[audio])

        return sorted(list(set(auditorium_audioformatlist)))

    def add_audio_non_security_device(self, audio_device_type):
        audio_non_security_device = self.normalize_settings.get('audio_non_security_device')
        manufacturer = audio_non_security_device[audio_device_type]['manufacturer']
        devicetypeid = audio_non_security_device[audio_device_type]['devicetypeid']
        device_serial = 'Not Real Device'
        device_name = '{0}_{1}_{2}_{3}'.format(manufacturer, audio_device_type, device_serial, devicetypeid)
        device = {
            "Capabilities": {},
            "DeviceIdentifier": {
                "CertThumbprint": None,
                "DeviceUID": "urn:uuid:{0}".format(str(uuid.uuid4())),
            },
            "DeviceTypeID": devicetypeid,
            "IsActive": True,
            "DeviceName": device_name.replace(" ", ""),
            "DeviceSerial": device_serial,
            "Manufacturer": manufacturer,
            "ModelNumber": audio_device_type
        }
        self.audio_non_security_devices.append(device.copy())

    # TODO needs rule file (Complete)
    def get_screen_luminance(self, screen_luminance):
        screen_luminance_dict = {}

        screen_luminance_units_values = self.normalize_settings.get('screen_luminance_units_values')
        if not screen_luminance:
            return screen_luminance_dict

        screen_luminance_units = screen_luminance.get('@units', None)

        if screen_luminance_units.lower() in screen_luminance_units_values:
            screen_luminance_dict['Units'] = screen_luminance_units.lower()
            screen_luminance_dict['Value'] = screen_luminance.get('#value', None)

        else:
            screen_luminance_dict['Units'] = 'other'
            screen_luminance_dict['Value'] = screen_luminance.get('#value', None)

        return screen_luminance_dict

    # TODO needs rule file (Complete)
    def get_screen_type(self, screen_type):
        screen_type_values = self.normalize_settings.get('screen_type_values')
        screen_type_value = None

        if not screen_type:
            return screen_type_value

        if '@scope' in screen_type:
            check_screen_type = screen_type.get('#value', None)
        elif '#value' in screen_type:
            check_screen_type = screen_type.get('#value', None)
        else:
            check_screen_type = screen_type

        for valid_screen_type in screen_type_values:
            if check_screen_type.upper() in screen_type_values[valid_screen_type]:
                screen_type_value = valid_screen_type
                return screen_type_value
            else:
                screen_type_value = 'Other'

        return screen_type_value

    def get_digital_3d_system(self, digital_3d_system):
        logger.info('We have our get_digital_3d_system in the class')
        digital_3d_system_dict = {}
        if not digital_3d_system:
            digital_3d_system_dict['ScreenType'] = self.get_screen_type(None)
            return digital_3d_system_dict

        digital_3d_system_dict['ScreenLuminance'] = self.get_screen_luminance(
            digital_3d_system.get('ScreenLuminance', None))
        digital_3d_system_dict['IsActive'] = self.get_boolean_value(digital_3d_system.get('IsActive', None))
        digital_3d_system_dict['InstallDate'] = self.format_date_string(digital_3d_system.get('InstallDate', None))
        digital_3d_system_dict['ScreenType'] = self.get_screen_type(digital_3d_system.get('ScreenType', None))
        digital_3d_system_dict['Digital3DConfiguration'] = self.get_scoped_variable(
            digital_3d_system.get('Digital3DConfiguration', None))

        return digital_3d_system_dict

    # Currently no normalization
    # This variable returned should be a flm:ScopedStringType
    def get_closed_caption_systems(self, closedcaptionsystem):
        logger.info('We have our get_closed_caption_systems in the class')
        if not closedcaptionsystem:
            return None
        else:
            return self.get_scoped_variable(closedcaptionsystem.get('Kind', None))

    # Currently no normalization
    # This variable returned should be a flm:ScopedStringType
    def get_visually_impaired_narration_system(self, visually_impaired):
        logger.info('We have our get_visually_impaired_narration_system in the class')
        if not visually_impaired:
            return None
        else:
            return self.get_scoped_variable(visually_impaired.get('Kind', None))

            # kind = visually_impaired.get('Kind',None)
            # visually_impaired_narration_dict['Kind'] = kind.get('#value', None)

    # return visually_impaired_narration_dict

    # Currently no normalization
    # This variable returned should be a flm:ScopedStringType
    def get_hearing_impaired_system(self, hearing_impaired):
        logger.info('We have our get_hearing_impaired_system in the class')
        if not hearing_impaired:
            return None
        else:
            return self.get_scoped_variable(hearing_impaired.get('Kind', None))

    # Currently no normalization for large format
    # This variable(Kind) returned should be a flm:ScopedStringType
    def get_large_format(self, large_format):
        logger.info('We have our get_large_format in the class')
        large_format_dict = {}
        if not large_format:
            return None
        else:
            large_format_dict['Value'] = self.get_scoped_variable(large_format.get('Kind', None))
            large_format_dict['InstallDate'] = self.format_date_string(large_format.get('InstallDate', None))

        return large_format_dict

    def get_auditorium_capabilities(self, auditorium_capabilities_dict):
        # logger.info('We have our get_auditorium_capabilities in the class')

        auditorium_capabilities = {}

        if not auditorium_capabilities_dict:
            # Send the default Null values for the UI tool
            auditorium_capabilities['ScreenAspectRatio'] = self.get_screen_aspect_ratio(None)
            auditorium_capabilities['AdjustableScreenMask'] = self.get_adjustable_screen_mask(None)
            auditorium_capabilities['Digital3DSystem'] = self.get_digital_3d_system(None)
            return auditorium_capabilities

        auditorium_capabilities['Supports35MM'] = self.get_boolean_value(
            auditorium_capabilities_dict.get('Supports35MM', None))
        auditorium_capabilities['ScreenAspectRatio'] = self.get_screen_aspect_ratio(
            self.get_scoped_variable(auditorium_capabilities_dict.get('ScreenAspectRatio', None)))
        auditorium_capabilities['AdjustableScreenMask'] = self.get_adjustable_screen_mask(
            auditorium_capabilities_dict.get('AdjustableScreenMask', None))
        auditorium_capabilities['AudioFormatList'] = self.get_audioformatlist(
            auditorium_capabilities_dict.get('AudioFormatList', None))
        auditorium_capabilities['Digital3DSystem'] = self.get_digital_3d_system(
            auditorium_capabilities_dict.get('Digital3DSystem', None))
        auditorium_capabilities['ClosedCaptionSystem'] = self.get_closed_caption_systems(
            auditorium_capabilities_dict.get('ClosedCaptionSystem', None))
        auditorium_capabilities['VisuallyImpairedNarrationSystem'] = self.get_visually_impaired_narration_system(
            auditorium_capabilities_dict.get('VisuallyImpairedNarrationSystem', None))
        auditorium_capabilities['HearingImpairedSystem'] = self.get_hearing_impaired_system(
            auditorium_capabilities_dict.get('HearingImpairedSystem', None))
        auditorium_capabilities['LargeFormat'] = self.get_large_format(
            auditorium_capabilities_dict.get('LargeFormat', None))

        return auditorium_capabilities

    def get_boolean_value(self, value):
        boolean_values = self.normalize_settings.get('boolean_values')
        if value is None:
            return None
        # Check to see which value
        if value in boolean_values['true']:
            return True
        elif value in boolean_values['false']:
            return False
        else:
            return None

    def get_device_indentifier(self, device_identifier):
        device_identifier_dict = dict()
        device_identifier_dict['CertThumbprint'] = None
        device_identifier_dict['DeviceUID'] = None

        if not device_identifier:
            return device_identifier_dict

        if device_identifier['@idtype'] in 'CertThumbprint':
            device_identifier_dict['CertThumbprint'] = device_identifier.get('#value', None)

        if device_identifier['@idtype'] in 'DeviceUID':
            device_identifier_dict['DeviceUID'] = device_identifier.get('#value', None)

        return device_identifier_dict

    # Value from the <AuditoriumList><Auditorium> <NonSecurityDeviceList>
    def get_non_security_device_list(self, nonsecuritydevice):
        logger.info('We have our get_non_security_device_list in the class')

        # add all the audio_non_security_devices from
        # from adding the audio format that was a atmos,auro or dtsx.
        non_securitydevicelist = list(self.audio_non_security_devices)

        # clear out the list from audio formats given
        self.audio_non_security_devices = []

        if not nonsecuritydevice:
            return non_securitydevicelist

        return non_securitydevicelist + self.get_devices(nonsecuritydevice.get('Device', None))

    # A Suite is a list of Devices
    def get_suite_list(self, suite_list):
        suite_details_dict = {}
        suitelist = []
        if not suite_list:
            return suitelist
        i = 1
        for suite in suite_list['Suite']:
            suite_details_dict['Device'] = self.get_devices(suite.get('Device', None), True)
            i = i + 1
            suitelist.append(suite_details_dict.copy())
        return suitelist

    def create_device_name(self, device_dict):
        device_name = '{0}_{1}_{2}_{3}'.format(device_dict['Manufacturer'],
                                               device_dict['ModelNumber'],
                                               device_dict['DeviceSerial'],
                                               device_dict['DeviceTypeID'])

        return device_name.replace(" ", "")

    def get_normalized_mms(self, device):
        normalized_dict = dict()

        device_id = device.get('DeviceTypeID', None)
        if device_id and device_id != '':
            normalized_dict['DeviceTypeID'] = device.get('DeviceTypeID', None)
        else:
            normalized_dict['DeviceTypeID'] = 'No_Device_Id'

        device_serial = device.get('DeviceSerial', None)
        if device_serial and device_serial != '':
            normalized_dict['DeviceSerial'] = device.get('DeviceSerial', None)
        else:
            normalized_dict['DeviceSerial'] = 'No_Device_Serial'

        manufacturer = self.get_scoped_variable(device.get('Manufacturer', None))
        if manufacturer and manufacturer != '':
            normalized_dict['Manufacturer'] = self.get_scoped_variable(device.get('Manufacturer', None))
        else:
            normalized_dict['Manufacturer'] = 'No_Device_Manufacturer'

        model_number = device.get('ModelNumber', None)
        if model_number and model_number != '':
            normalized_dict['ModelNumber'] = device.get('ModelNumber', None)
        else:
            normalized_dict['ModelNumber'] = 'No_Device_Model'

        if self.get_scoped_variable(device.get('Manufacturer', None)):
            current_manufacturer = self.get_scoped_variable(device.get('Manufacturer', None))
        else:
            current_manufacturer = 'No_Device_Manufacturer'

        current_model = device.get('ModelNumber', None)
        normalized_dict['FeedModelNumber'] = current_model

        manufacture_names = self.integrator_rules.get('manufacture_names')
        integrator_feed = self.integrator_rules.get('integrator_feed')
        for names in manufacture_names:
            if current_manufacturer.strip().upper() in manufacture_names[names]:
                normalized_dict['Manufacturer'] = names

                # Get the normalized Model name
        model_names = self.integrator_rules.get(str(normalized_dict['Manufacturer']))

        if not model_names:
            # TODO: We need to log here, so we can know we do not have (COMPLETE)
            logger.info('FLM MODEL not normalized using feedModelname [{0}] from FEED [{1} from MAKE [{2}]]'.
                        format(current_model, integrator_feed, normalized_dict['Manufacturer']))
            # mapping with the integrator
            return normalized_dict

        for model in model_names:
            if current_model.strip().upper() in model_names[model]:
                normalized_dict['ModelNumber'] = model
        return normalized_dict

    def get_devices(self, devices, suite_device=False):
        device_list = []
        if not devices:
            return device_list
        for device in devices:
            device_dict = {}

            device_dict['DeviceTypeID'] = device.get('DeviceTypeID', None)
            device_dict['DeviceSerial'] = device.get('DeviceSerial', None)
            device_dict['Manufacturer'] = self.get_scoped_variable(device.get('Manufacturer', None))
            device_dict['ModelNumber'] = device.get('ModelNumber', None)

            # We need to normalize the Make, Model,and possibly Serial when it comes from Suite Device
            if suite_device:
                device_dict = self.get_normalized_mms(device_dict)

            # Create the name
            device_name = self.create_device_name(device_dict)

            # If the name comes back wih No DeviceTypeID,DeviceSerial,Manufacturer,ModelNumber
            # skip this device, this device cannot be used.
            if 'No_Device_Manufacturer_No_Device_Model_No_Device_Serial' in device_name or 'No_Device_Serial' in device_name:
                continue

            device_dict['DeviceName'] = device_name

            device_dict['InstallDate'] = self.format_date_string(device.get('InstallDate', None))

            device_dict['IsActive'] = self.get_boolean_value(device.get('IsActive', None))
            device_dict['Integrator'] = self.get_scoped_variable(device.get('Integrator', None))
            device_dict['VPFFinanceEntity'] = self.get_scoped_variable(device.get('VPFFinanceEntity', None))
            device_dict['VPFStartDate'] = self.format_date_string(device.get('VPFStartDate', None))

            device_dict['DeviceIdentifier'] = self.get_device_indentifier(device.get('DeviceIdentifier', None))
            device_dict['Component'] = self.get_device_components(device.get('ComponentList', None))

            # TODO revisit how to handle this.
            # device_dict['KeyInfoList'] = self.get_keyinfo_list(device.get('KeyInfoList',None))

            device_dict['Capabilities'] = self.get_device_capabilities(device.get('Capabilities', None))

            device_list.append(device_dict.copy())

        return device_list

    def get_scoped_variable(self, variable):
        if not variable:
            return None
        if '@scope' in variable:
            return variable.get('#value', None)
        else:
            return variable

    def get_user_text_variable(self, variable):
        if not variable:
            return None
        if '@language' in variable:
            return variable.get('#value', None)
        else:
            return variable

    def get_device_components(self, components):
        components_list = []
        if not components:
            return components_list

        for component in components['Component']:
            component_dict = dict()
            component_dict['Description'] = component.get('Description', None)
            component_dict['Version'] = component.get('Version', None)

            component_dict['ComponentKind'] = self.get_component_kind(component.get('ComponentKind', None))
            component_dict['ComponentManufacturer'] = self.get_scoped_variable(
                component.get('ComponentManufacturer', None))

            components_list.append(component_dict.copy())
        return components_list

    # TODO needs rule file (COMPLETE)
    def get_component_kind(self, component_kind):
        value = None
        component_value = ''
        if not component_kind:
            return value

        component_kind_accepted = self.normalize_settings.get('component_kind_values')

        # Check to see if scope exists
        if '@scope' in component_kind:
            value = component_kind.get('#value', None)
        else:
            value = component_kind

        # verify the component kind is valid
        if value:
            for component in component_kind_accepted:

                if value.upper() in component_kind_accepted[component]:
                    component_value = component
                    return component_value
                else:
                    component_value = ''

        return component_value

    # TODO: There should be a list of KeyInfo, but currently not defined
    # if should be looping the KeyInfoList once we get the correct format
    def get_keyinfo_list(self, keyinfo):
        keyinfo_list = []
        keyinfo_dict = dict()
        if not keyinfo:
            return keyinfo_list

        x509_data = keyinfo['KeyInfo'].get('X509Data', None)
        x509_data_dict = {}

        if isinstance(x509_data, list):
            x509_data_dict['X509Data'] = self.get_x509_data_list(x509_data)
            keyinfo_dict['KeyInfo'] = x509_data_dict['X509Data']
            keyinfo_list.append(keyinfo_dict.copy())

        return keyinfo_list

    def get_x509_data_list(self, x509_data):
        keyinfo_list = []
        for info in x509_data:
            keyinfo_dict = dict()
            keyinfo_dict['X509Certificate'] = info.get('X509Certificate', None)
            keyinfo_dict['X509SubjectName'] = info.get('X509SubjectName', None)
            keyinfo_list.append(keyinfo_dict.copy())

        return keyinfo_list

    def get_device_capabilities(self, capabilities):
        capabilities_dict = dict()
        if not capabilities:
            return capabilities_dict
        capabilities_dict['Resolution'] = self.get_scoped_variable(capabilities.get('Resolution', None))
        capabilities_dict['WatermarkingList'] = self.get_watermarking_list(capabilities.get('WatermarkingList', None))

        return capabilities_dict

    def get_watermarking_list(self, watermarkings):
        watermarking_list = []
        if not watermarkings:
            return watermarking_list
        for watermarking in watermarkings['Watermarking']:
            watermarking_dict = {}
            watermarking_dict['WatermarkManufacturer'] = self.get_scoped_variable(
                watermarking.get('WatermarkManufacturer', None))
            watermarking_dict['WatermarkKind'] = self.get_water_mark_kind(watermarking.get('WatermarkKind', None))
            watermarking_dict['WatermarkModel'] = watermarking.get('WatermarkModel', None)
            watermarking_dict['WatermarkVersion'] = watermarking.get('WatermarkVersion', None)

            watermarking_list.append(watermarking_dict.copy())
        return watermarking_list

    def get_water_mark_kind(self, watermark_kind):
        value = None
        watermark_value = ''
        if not watermark_kind:
            return value

        watermark_kind_values = self.normalize_settings.get('watermark_kind_values')

        # Check to see if scope exists
        if '@scope' in watermark_kind:
            value = watermark_kind.get('#value', None)
        else:
            value = watermark_kind
        if value:
            for watermark in watermark_kind_values:

                # verify the component kind is valid
                if value.upper() in watermark_kind_values[watermark]:
                    return watermark

        return watermark_value
