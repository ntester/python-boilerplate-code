# parse_normalize_json

A coding example how to parse a JSON object and create a new JSON object with
fields normalized to conditions set in config directory.


## Installation

Use Python 3.11.2

```
python3 -m venv env311

```

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install python packages from requirements.txt file.

```
pip install -r requirements.txt

```

Use this command to run test cases using unnitest.

```
 python -m unittest discover test

```




