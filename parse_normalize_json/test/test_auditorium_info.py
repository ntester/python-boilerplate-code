import unittest
import json
import os

from normalize_flm  import FlmNormalize

# Use for looking at data
from pprint import pprint
import pprint
pp = pprint.PrettyPrinter(indent=1)


class TestAuditorium(unittest.TestCase):

    def setUp(self):
        self.test_data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),'test_data')

    def test_full_auditorium_info(self):
        self.flm_normalize = FlmNormalize()
        auditorium_info = {}
        with open(os.path.join(self.test_data_path,'auditorium_info.json')) as json_data:
            auditorium_info_jason = json.load(json_data)
        auditorium_info = auditorium_info_jason['AuditoriumList']
        auditorium_list = auditorium_info.get('Auditorium',None)
        auditorium = auditorium_list[0]

        auditorium_number_or_name = auditorium.get('AuditoriumNumberOrName',None)
        auditorium_install_date = auditorium.get('AuditoriumInstallDate',None)

        seating_capacity = auditorium.get('SeatingCapacity',None)
        screenwidth =  self.flm_normalize.get_screenwidth(auditorium.get('ScreenWidth',None))
        capabilities = self.flm_normalize.get_auditorium_capabilities(auditorium.get('Capabilities',None))
        adjustable_screen_mask = self.flm_normalize.get_adjustable_screen_mask(capabilities.get('AdjustableScreenMask',None) )
        suite_list = self.flm_normalize.get_suite_list(auditorium.get('SuiteList',None))
        non_security_device_list = self.flm_normalize.get_non_security_device_list(auditorium.get('NonSecurityDeviceList',None))


        # Straight check to see if test file was parsed correctly and Normalized
        self.assertEqual(auditorium_number_or_name, 'Screen 01')
        self.assertEqual(auditorium_install_date, '2012-09-12')
        self.assertEqual(seating_capacity, 101)
        self.assertEqual(screenwidth['Units'], 'foot')
        self.assertEqual(screenwidth['Value'], 52)
        self.assertEqual(capabilities['Supports35MM'], False)
        self.assertEqual(adjustable_screen_mask, 'Top')
        self.assertEqual(non_security_device_list[0]['ModelNumber'], 'DTSX')
        self.assertEqual(suite_list[0]['Device'][0]['VPFFinanceEntity'], 'AAM-VPF')
        self.assertEqual(suite_list[0]['Device'][0]['DeviceSerial'], '252039')

    def test_auditorium_capabilities(self):

        self.flm_normalize = FlmNormalize()

        with open(os.path.join(self.test_data_path,'auditorium_capabilities.json')) as json_data:
            auditorium_capabilities_jason = json.load(json_data)

        capabilities = self.flm_normalize.get_auditorium_capabilities(auditorium_capabilities_jason['Capabilities'])

        self.assertEqual(capabilities['AdjustableScreenMask'], 'Top')
        self.assertEqual(len(capabilities['AudioFormatList']), 3 )
        self.assertEqual(capabilities['Digital3DSystem']['ScreenLuminance']['Units'], 'foot-lambert')
        self.assertEqual(capabilities['VisuallyImpairedNarrationSystem'], 'VI-System-Name/Model')

    def test_auditorium_screenwidth(self):
        self.flm_normalize = FlmNormalize()
        with open(os.path.join(self.test_data_path,'screen_width.json')) as json_data:
            screen_width_jason = json.load(json_data)
        screen_width = self.flm_normalize.get_screenwidth(screen_width_jason['ScreenWidth'])

        # if ScreenWidth @units value is not foot, meter, inch, or centimeter 
        # value will be returned as ''
        self.assertEqual(screen_width['Units'], '')

    def test_suite_list(self):
        self.flm_normalize = FlmNormalize()
        with open(os.path.join(self.test_data_path,'suite_list.json')) as json_data:
            suite_list_jason = json.load(json_data)
        suite_list = self.flm_normalize.get_suite_list(suite_list_jason['SuiteList'])

        self.assertEqual(len(suite_list), 2 )
        self.assertEqual(suite_list[0]['Device'][0]['DeviceSerial'], '000100' )


    def test_non_security_devices(self):
        self.flm_normalize = FlmNormalize()
        with open(os.path.join(self.test_data_path,'non_security_devices.json')) as json_data:
            non_security_devices_json = json.load(json_data)

        non_security_device_list = self.flm_normalize.get_non_security_device_list(non_security_devices_json['NonSecurityDeviceList'])

        device_make = ('BARCO','DOREMI')
        for device in non_security_device_list:
             self.assertIn(device['Manufacturer'],device_make)



    def test_devices(self):
        self.flm_normalize = FlmNormalize()
        with open(os.path.join(self.test_data_path, 'new_suite_list.json')) as json_data:
            suite_list_json = json.load(json_data)
        suite_list = self.flm_normalize.get_suite_list(suite_list_json['SuiteList'])

        for device in suite_list:
            device_name = device['Device'][0]['DeviceName']
            self.assertIn(device_name,'CHRISTIE_CP2230_247869009_PR')


suite = unittest.TestLoader().loadTestsFromTestCase(TestAuditorium)
unittest.TextTestRunner(verbosity=2).run(suite)
