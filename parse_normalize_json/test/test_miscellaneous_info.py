import unittest
import json
import os


from deepdiff import DeepDiff
from jsondiff import diff

from normalize_flm  import FlmNormalize

from pprint import pprint
import pprint

class TestMiscellaneous(unittest.TestCase):

    def setUp(self):
        self.test_data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),'test_data')


    def test_boolean_values(self):
        self.flm_normalize = FlmNormalize()
        self.assertEqual(self.flm_normalize.get_boolean_value('true'), True)
        self.assertEqual(self.flm_normalize.get_boolean_value('blah'), None)
        self.assertEqual(self.flm_normalize.get_boolean_value(False), False)
        self.assertEqual(self.flm_normalize.get_boolean_value('N'), False)

    def test_get_component_kind(self):
        self.flm_normalize = FlmNormalize()
        self.assertEqual(self.flm_normalize.get_component_kind('HARD'), 'Hardware')
        self.assertEqual(self.flm_normalize.get_component_kind('KHARD'), '')
        self.assertEqual(self.flm_normalize.get_component_kind('software'), 'Software')
        self.assertEqual(self.flm_normalize.get_component_kind('FMWH'), 'Firmware')

    def test_get_water_mark_kind(self):
        self.flm_normalize = FlmNormalize()
        self.assertEqual(self.flm_normalize.get_water_mark_kind('PiCtUrE'), 'Picture')
        self.assertEqual(self.flm_normalize.get_water_mark_kind('Audio'), 'Audio')
        self.assertEqual(self.flm_normalize.get_water_mark_kind('blah'), '')
    
    def test_get_screen_type(self):
        self.flm_normalize = FlmNormalize()
        self.assertEqual(self.flm_normalize.get_screen_type('SILVER'), 'Silver')
        self.assertEqual(self.flm_normalize.get_screen_type('GREY'), 'Silver')
        self.assertEqual(self.flm_normalize.get_screen_type('Wt'), 'White')
        self.assertEqual(self.flm_normalize.get_screen_type('KK'), 'Other')

    def test_get_adjustable_screen_mask(self):
        self.flm_normalize = FlmNormalize()
        self.assertEqual(self.flm_normalize.get_adjustable_screen_mask('FloatingFlat'), 'FloatingFlat')
        self.assertEqual(self.flm_normalize.get_adjustable_screen_mask('sidetop'), 'SideTop')

    def test_get_audioformatlist(self):
        self.flm_normalize = FlmNormalize()
        with open(os.path.join(self.test_data_path,'audio_format_list.json')) as json_data:
            audio_format_jason = json.load(json_data)
        audio_format_list = audio_format_jason['AudioFormatList']
        audio_list = self.flm_normalize.get_audioformatlist(audio_format_list)
        self.assertIn('M', audio_list)
        self.assertIn('71', audio_list)
        self.assertNotIn('ATMOS', audio_list)

    def test_get_screen_aspect_ratio(self):
        self.flm_normalize = FlmNormalize()
        self.assertEqual( self.flm_normalize.get_screen_aspect_ratio('185')['Value'], '1.85')
        self.assertEqual( self.flm_normalize.get_screen_aspect_ratio('885')['Value'], 'Other')

    def test_set_integrator(self):
        self.flm_normalize = FlmNormalize()
        self.assertEqual( self.flm_normalize.set_integrator('urn:x-facilityID:aam.com:ZM-SKK-010101-01'),'aam') 
        self.assertEqual(self.flm_normalize.set_integrator('urn:x-facilityID:KKKaam.com:ZM-SKK-010101-01'),'unhandled integrator')
        self.assertEqual(self.flm_normalize.set_integrator('urn:x-facilityID:cinemark.com:416'),'dcip')

    def test_get_normalized_facility_id(self):
        self.flm_normalize = FlmNormalize()
        id = self.flm_normalize.get_normalized_facility_id('urn:x-facilityID:aam.com:ZM-SKK-010101-01')
        self.assertEqual(self.flm_normalize.get_normalized_facility_id('urn:x-facilityID:aam.com:ZM-SKK-010101-01'),'aam.com:ZM-SKK-010101-01')
        self.assertEqual(self.flm_normalize.get_normalized_facility_id('urn:x-facilityID:cinemark.com:416'),'cinemark.com:416')

    
    def test_smpte_facilities_capabilities(self):
        self.flm_normalize = FlmNormalize()
        capabilities = {}

        with open(os.path.join(self.test_data_path,'smpte_facilities_capabilities.json')) as json_data:
            f_capabilities_info_jason = json.load(json_data)
        f_capabilities = f_capabilities_info_jason['Capabilities']
        kdm_delivery_method_list = f_capabilities['KDMDeliveryMethodList']
        dcp_delivery_method_list = f_capabilities['DCPDeliveryMethodList']

        kdm_smpte_delivery_methods = self.flm_normalize.get_smpte_delivery_methods(kdm_delivery_method_list)
        dcp_smpte_delivery_methods = self.flm_normalize.get_smpte_delivery_methods(dcp_delivery_method_list)

        self.assertEqual(kdm_smpte_delivery_methods['TKR'], True)
        self.assertEqual(len(kdm_smpte_delivery_methods['Satellite']), 2)
        self.assertEqual(kdm_smpte_delivery_methods['Satellite'][0]['Provider'], 'Name of Satellite Company')

        self.assertEqual(len(dcp_smpte_delivery_methods['Network']), 2)
        self.assertEqual(dcp_smpte_delivery_methods['Satellite'][0]['Provider'], 'Name of Satellite Company DCP')

    def test_get_interop_delivery_methods_with_smpte_file(self):
        self.flm_normalize = FlmNormalize()
        capabilities = {}

        with open(os.path.join(self.test_data_path,'smpte_facilities_capabilities.json')) as json_data:
            f_capabilities_info_jason = json.load(json_data)
        f_capabilities = f_capabilities_info_jason['Capabilities']
        kdm_delivery_method_list = f_capabilities['KDMDeliveryMethodList']
        kdm_interop_delivery_methods = self.flm_normalize.get_interop_delivery_methods(kdm_delivery_method_list)
        self.assertEqual(kdm_interop_delivery_methods['TKR'], True)

    def test_get_interop_delivery_methods(self):
        self.flm_normalize = FlmNormalize()
        capabilities = {}

        with open(os.path.join(self.test_data_path,'interop_facilities_capabilities.json')) as json_data:
            f_capabilities_info_jason = json.load(json_data)
        f_capabilities = f_capabilities_info_jason['Capabilities']
        kdm_delivery_method_list = f_capabilities['KDMDeliveryMethodList']

        kdm_interop_delivery_methods = self.flm_normalize.get_interop_delivery_methods(kdm_delivery_method_list)
        self.assertEqual(kdm_interop_delivery_methods['TKR'], True)

    def test_boolean_vars(self):
        self.flm_normalize = FlmNormalize()
        auditorium_info = {}

        with open(os.path.join(self.test_data_path,'auditorium_info.json')) as json_data:
            auditorium_info_jason = json.load(json_data)
        auditorium_info = auditorium_info_jason['AuditoriumList']
        auditorium_list = auditorium_info.get('Auditorium',None)
        auditorium = auditorium_list[0]
        capabilities = self.flm_normalize.get_auditorium_capabilities(auditorium.get('Capabilities',None))
        self.assertEqual(capabilities['Supports35MM'], False)
        self.assertEqual(capabilities['Digital3DSystem']['IsActive'], True)

    def test_facility_diff(self):
        self.flm_normalize = FlmNormalize()
        complete_facility_info = {}
        diff_complete_facility_info = {}

        # Print to show users how the DeepDiff module works if needed
        print_diff = False
        pp = pprint.PrettyPrinter(indent=1)

        with open(os.path.join(self.test_data_path,'facility_info.json')) as json_data:
            facility_info_json = json.load(json_data)
        facility_info = facility_info_json['FacilityInfo']

        with open(os.path.join(self.test_data_path,'diff_facility_info.json')) as json_data:
            diff_facility_info_json = json.load(json_data)
        diff_facility_info = diff_facility_info_json['FacilityInfo']

        # Facility info
        complete_facility_info['FacilityName']= self.flm_normalize.get_user_text_variable(facility_info.get('FacilityName',None))
        complete_facility_info['Circuit']= self.flm_normalize.get_user_text_variable(facility_info.get('Circuit',None))
        complete_facility_info['Addresses'] = self.flm_normalize.get_address_list(facility_info.get('AddressList',None))
        complete_facility_info['AlternateFacilityIDList']= self.flm_normalize.get_alternate_facility_id_list(facility_info.get('AlternateFacilityIDList',None))
        complete_facility_info['Facility_Capabilities'] = self.flm_normalize.get_facility_capabilities(facility_info.get('Capabilities',None))
        complete_facility_info['ContactList']=  self.flm_normalize.get_contact_list(facility_info.get('ContactList',None))
        complete_facility_info['FacilityDeviceList']= self.flm_normalize.get_facility_device_list(facility_info.get('DeviceList',None))
        complete_facility_info['FacilityTimeZone'] = self.flm_normalize.get_facility_time_zone(facility_info.get('FacilityTimeZone',None))

        # diff facility info
        diff_complete_facility_info['FacilityName']= self.flm_normalize.get_user_text_variable(diff_facility_info.get('FacilityName',None))
        diff_complete_facility_info['Circuit']= self.flm_normalize.get_user_text_variable(diff_facility_info.get('Circuit',None))
        diff_complete_facility_info['Addresses'] = self.flm_normalize.get_address_list(diff_facility_info.get('AddressList',None))
        diff_complete_facility_info['AlternateFacilityIDList']= self.flm_normalize.get_alternate_facility_id_list(diff_facility_info.get('AlternateFacilityIDList',None))
        diff_complete_facility_info['Facility_Capabilities'] = self.flm_normalize.get_facility_capabilities(diff_facility_info.get('Capabilities',None))
        diff_complete_facility_info['ContactList']=  self.flm_normalize.get_contact_list(diff_facility_info.get('ContactList',None))
        diff_complete_facility_info['FacilityDeviceList']= self.flm_normalize.get_facility_device_list(diff_facility_info.get('DeviceList',None))
        diff_complete_facility_info['FacilityTimeZone'] = self.flm_normalize.get_facility_time_zone(diff_facility_info.get('FacilityTimeZone',None))


        #example of DeepDiff
        deepdiff = DeepDiff(complete_facility_info, diff_complete_facility_info,verbose_level=2, view='tree' )
        if print_diff:
            print('\n\nHere is DeepDiff\n')
            pp.pprint(deepdiff)
            print('\nEND\n')

        # take a look how to parse the diff.
        if 'iterable_item_removed' in deepdiff:
            removed = list(deepdiff['iterable_item_removed'])
            for remove in removed:
                if print_diff:
                    print('\nthis is Remove list')
                    print(remove.path())
                    print('Original [{0}]'.format(remove.t1['URL']))
                    print('New [{0}]'.format(remove.t2))
                    print('\n')
                original = remove.t1
                new = remove.t2

            self.assertEqual(str(original['URL']), 'sftp://username:password@kdm_upload.facility.example.com/kdm')
            self.assertEqual(str(new).upper(), 'Not Present'.upper())

        if 'iterable_item_added' in deepdiff:
            added = list(deepdiff['iterable_item_added'])
            for add in added:
                if print_diff:
                    print('\nthis is the Added list')
                    print(add.path())
                    print('Original [{0}]'.format(add.t1))
                    print('New [{0}]'.format(add.t2))
                    print('\n')
                original = add.t1
                new = add.t2
                self.assertEqual(str(original).upper(), 'Not Present'.upper())
                self.assertEqual(str(new['EmailAddress']) , 'JillDunlaoghaire@imccinemagroup.com')

        if 'values_changed' in deepdiff:
            changed = list(deepdiff['values_changed'])
            for change in changed:
                if print_diff:
                    print('\nThis is the Change list')
                    print(change.path())
                    print('Original [{0}]'.format(change.t1))
                    print('New [{0}]'.format(change.t2))
                    print('\n')
                original = change.t1
                new = change.t2
                self.assertEqual(original, 'USB')
                self.assertEqual(str(new) , 'USBSTICK')

    def test_format_date(self):
        self.flm_normalize = FlmNormalize()
        date_str_1 = '10-FEB-10'
        date_str_2 = '2010-03-10'
        date_str_3 = '2010-10-03'
        date_str_4 = '03.10.2010'
        date_str_5 = '2010.03.10'
        date_str_6 = '10.03.2010'
        date_str_7 = '16.03.2010'
        date_str_8 = '16.16.2010' 
        date_str_9 = None 

        value_1 = self.flm_normalize.format_date_string(date_str_1)
        value_2 = self.flm_normalize.format_date_string(date_str_2)
        value_3 = self.flm_normalize.format_date_string(date_str_3)
        value_4 = self.flm_normalize.format_date_string(date_str_4)
        value_5 = self.flm_normalize.format_date_string(date_str_5)
        value_6 = self.flm_normalize.format_date_string(date_str_6)
        value_7 = self.flm_normalize.format_date_string(date_str_7)
        value_8 = self.flm_normalize.format_date_string(date_str_8)
        value_9 = self.flm_normalize.format_date_string(date_str_9)

        self.assertEqual(value_1, '2010-02-10')
        self.assertEqual(value_2, '2010-03-10')
        self.assertEqual(value_3, '2010-10-03')
        self.assertEqual(value_4, '2010-03-10')
        self.assertEqual(value_5, '2010-03-10')
        self.assertEqual(value_6, '2010-10-03')
        self.assertEqual(value_7, '2010-03-16')
        self.assertEqual(value_8, '1970-01-01')
        self.assertEqual(value_9, None)

        print_dates = False 
        if print_dates:
            print('\nThe date format value is [{0}] for [{1}]'.format(value_1,date_str_1))
            print('\nThe date format value is [{0}] for [{1}]'.format(value_2,date_str_2))
            print('\nThe date format value is [{0}] for [{1}]'.format(value_3,date_str_3))
            print('\nThe date format value is [{0}] for [{1}]'.format(value_4,date_str_4))
            print('\nThe date format value is [{0}] for [{1}]'.format(value_5,date_str_5))
            print('\nThe date format value is [{0}] for [{1}]'.format(value_6,date_str_6))
            print('\nThe date format value is [{0}] for [{1}]'.format(value_7,date_str_7))
            print('\nThe date format value is [{0}] for [{1}]'.format(value_8,date_str_8))
            print('\nThe date format value is [{0}] for [{1}]'.format(value_9,date_str_9))
        

suite = unittest.TestLoader().loadTestsFromTestCase(TestMiscellaneous)
unittest.TextTestRunner(verbosity=2).run(suite)
