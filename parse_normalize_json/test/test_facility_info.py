import unittest
import json
import os

from normalize_flm  import FlmNormalize

class TestFacility(unittest.TestCase):

    def setUp(self):
        self.test_data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),'test_data')

    def test_full_facility_info(self):
        self.flm_normalize = FlmNormalize()
        facility_info = {}
        return_value = {}
        with open(os.path.join(self.test_data_path,'facility_info.json')) as json_data:

            facility_info_json = json.load(json_data)
        facility_info = facility_info_json['FacilityInfo']

        facility_name = self.flm_normalize.get_user_text_variable(facility_info.get('FacilityName',None))
        circuit = self.flm_normalize.get_user_text_variable(facility_info.get('Circuit',None))


        # test the addresses
        address_dict =  facility_info['AddressList']
        physical_address = self.flm_normalize.get_address(address_dict.get('Physical',None))
        shipping_address = self.flm_normalize.get_address(address_dict.get('Shipping',None))
        billing_address = self.flm_normalize.get_address(address_dict.get('Billing',None))


        alternate_facility_id_list =  self.flm_normalize.get_alternate_facility_id_list(facility_info.get('AlternateFacilityIDList',None))
        facility_capabilities =  self.flm_normalize.get_facility_capabilities(facility_info.get('Capabilities',None))
        contact_list=  self.flm_normalize.get_contact_list(facility_info.get('ContactList',None))
        facility_device_list = self.flm_normalize.get_facility_device_list(facility_info.get('DeviceList',None))
        facility_time_zone =  self.flm_normalize.get_facility_time_zone(facility_info.get('FacilityTimeZone',None))


        # Straight check to see if values were parsed correctly 
        self.assertEqual(facility_name, 'CineStar Crimmitschau')
        self.assertEqual(circuit, 'CineStar')
        self.assertEqual(physical_address['City'], 'Crimmitschau')
        self.assertEqual(shipping_address['City'], 'Berlin')
        self.assertEqual(billing_address['City'], 'Dortmund')
        self.assertEqual(alternate_facility_id_list[0]['AlternateFacilityID'], 'fox.com:IE-6202')
        self.assertEqual(facility_capabilities['KDMDeliveryMethodList']['Physical'][0]['MediaType'], 'USB')
        self.assertEqual(contact_list[0]['Email'], 'Crimmitschau@CineStar.de')
        self.assertEqual(facility_device_list[0]['Manufacturer'], 'Example Manufacturer 2')
        self.assertEqual(facility_time_zone, 'Europe/Berlin')



    def test_contact_list(self):
        self.flm_normalize = FlmNormalize()
        complete_facility_info = {}
        facility_info = {}
        with open(os.path.join(self.test_data_path,'contact.json')) as json_data:
            content_json_data = json.load(json_data)
        facility_info['ContactList'] = content_json_data['ContactList']
        complete_facility_info['ContactList']=  self.flm_normalize.get_contact_list(facility_info.get('ContactList',None))
        test_name = complete_facility_info['ContactList'][0]['Name']
        self.assertEqual(test_name, 'Jane Doe')
       
    def test_bad_email(self):
        self.flm_normalize = FlmNormalize()
        complete_facility_info = {}
        facility_info = {}
        with open(os.path.join(self.test_data_path,'bad_email_contact.json')) as json_data:
            content_json_data = json.load(json_data)
        facility_info['ContactList'] = content_json_data['ContactList']
        complete_facility_info['ContactList']=  self.flm_normalize.get_contact_list(facility_info.get('ContactList',None))
        test_name = complete_facility_info['ContactList'][0]['Name']
        self.assertNotEqual(test_name, 'Jane Doe')
        if complete_facility_info['ContactList'][0]:
            test_bad_email = complete_facility_info['ContactList'][0].get('Email','')
            self.assertEqual(test_bad_email, '')
       
    def test_device(self):
        self.flm_normalize = FlmNormalize()
        complete_facility_info = {}
        with open(os.path.join(self.test_data_path,'complete_device.json')) as json_data:
            device_json_data = json.load(json_data)

        device = device_json_data['Device']
        devices = self.flm_normalize.get_devices(device)

        self.assertEqual(devices[0]['ModelNumber'], 'DS1000')
       
    def test_facility_device(self):
        self.flm_normalize = FlmNormalize()
        complete_facility_info = {}
        with open(os.path.join(self.test_data_path,'facility_device.json')) as json_data:
            facility_device_json_data = json.load(json_data)

        devices = self.flm_normalize.get_facility_device_list(facility_device_json_data['DeviceList'])

        self.assertEqual(len(devices), 2)
        self.assertEqual(devices[0]['Integrator'], None)
        self.assertNotEqual(devices[1]['DeviceIdentifier']['CertThumbprint'], None)
       

suite = unittest.TestLoader().loadTestsFromTestCase(TestFacility)
unittest.TextTestRunner(verbosity=2).run(suite)
